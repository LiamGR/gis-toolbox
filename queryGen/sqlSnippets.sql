#Count the number of polygons from qryTbl in each categorey of catTbl
#Count is filtered to return different values (i.e. count different areas)
SELECT 	catTbl.categorey,
		COUNT(*) FILTER (WHERE ) AS count1, 
		COUNT(*) FILTER (WHERE ) AS count2, 
		COUNT(*) FILTER (WHERE ) AS count3, 
FROM catTbl JOIN qryTbl ON ST_Intersects(catTbl.geom,qryTbl.geom)
GROUP BY catTbl.categorey
#AVG(ST_Area(qryTbl.geom)) FILTER (WHERE ) AS avarea1 as alternative


#Histogram of areas
SELECT width_bucket(st_area(st_transform(geom,3308))/10000,0,1000,20) bucket,
count(*)
FROM tbl1
GROUP BY bucket
ORDER BY bucket