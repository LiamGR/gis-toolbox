import math,numpy
from qgis.core import *
class CentroidShape(object):
	def __init__(self,x,y):
		self.x = x
		self.y = y
		#Not sure about this....
		self.coordArray = None
	def straightLine(self, length):
		"""Creates straight line along the x axis"""
		self.coordArray = numpy.array([(self.x-(length/2),self.y),(self.x+(length/2),self.y)])

	def rectangle(self,width,height):
		"""
			Return array of coordinates for a four sided shape with 90 
			angles at specified distances between them
		"""
		x1 = self.x - width
		x2 = self.x + width
		y1 = self.y - height
		y2 = self.y + height
		self.coordArray = numpy.array([(x1,y1),(x2,y1),(x2,y2),(x1,y2)])

	def regularPolygonFromRadius(self,radius,nSides):
		"""
			Return array of coordinates for a regular polygon.
			Each corner of the polygon is the radius distance
			from this objects (x,y) coordinate.
		 """
		startPoint = (self.x,(self.y + radius))
		pointArray = []
		theta = 2*math.pi/nSides
		for i in range(nSides):
			xVal = math.cos(theta * i) * radius + self.x
			yVal = math.sin(theta * i) * radius + self.y
			pointArray.append((xVal,yVal))
		self.coordArray = (pointArray)
	def regularPolygonFromArea(self,area,nSides):
		"""
			Returns array of coordinates for a regular polygon
			with a specified area. Utilises the polygon from 
			radius function.
			Radius is found by using forumlar for equilateral triangle:
				area = 1/2 nSides * radius ^ 2 * sin(360/nSides)
			This is then the radius used in polygon from radius function
		"""
		theta = math.radians(360/nSides)
		radius = ((2*area)/(nSides*math.sin(theta)))
		print(radius)
		self.regularPolygonFromRadius(radius,nSides)

	def rotateAboutCentroid(self,theta):
		"""
			Rotate the polygon array of this object about the centroid
		"""
		rotateMatrix = numpy.array([(math.cos(theta),math.sin(theta)*-1),(math.sin(theta),math.cos(theta))])
		for i in range(len(self.coordArray)):
			xy = numpy.array([self.coordArray[i][0]-self.x,self.coordArray[i][1]-self.y])
			rotateXy = numpy.dot(rotateMatrix, xy)
			rotPt = (rotateXy[0] + self.x,rotateXy[1] +self.y)
			self.coordArray[i] = rotPt

	def rotateAboutPoint(self,theta,x,y):
		"""
			Rotate the polygon array of this object about a given point
		"""
		rotateMatrix = numpy.array([(math.cos(theta),math.sin(theta)*-1),(math.sin(theta),math.cos(theta))])
		for i in range(len(self.coordArray)):
			xy = numpy.array([self.coordArray[i][0]-x,self.coordArray[i][1]-y])
			rotateXy = numpy.dot(rotateMatrix, xy)
			rotPt = (rotateXy[0] + x,rotateXy[1] +y)
			self.coordArray[i] = rotPt

	def toWkt(self):
		"""
			Convert the coordinate array to polygon.
		"""
		wkt = ""
		#Only two coordinates produces a line
		if len(self.coordArray) == 2:
			wkt = ("LINESTRING({} {}, {} {}) ").format(str(self.coordArray[0][0]),str(self.coordArray[0][1]),str(self.coordArray[1][0]),str(self.coordArray[1][1]))
		#More than two coordinates produces polygon
		elif len(self.coordArray) >2:
			wkt = "POLYGON(("
			pts = ""
			for pt in self.coordArray:
				pts += str(pt[0]) + ' ' + str(pt[1]) + ','
			wkt+=pts[:-1] + '))'
		return wkt

	def toQgis(self):
		"""
			Load into QGIS session as memory layer
		"""
		asWkt = self.toWkt()
		geom = QgsGeometry.fromWkt(asWkt)
		gtype = 'Linestring' if geom.type() == 1 else 'Polygon'
		memLayer = QgsVectorLayer(gtype,"temp_layer","memory")
		dataProvider = memLayer.dataProvider()
		feature = QgsFeature()
		feature.setGeometry(geom)
		dataProvider.addFeatures([feature])
		QgsMapLayerRegistry.instance().addMapLayer(memLayer)
		


cp = CentroidShape(25,25)
cp.straightLine(10)
cp.rotateAboutCentroid(45)
# cp.regularPolygonFromRadius(20,6)
cp.toQgis()
# cp.rotateAboutCentroid(30)
# cp.toQgis()